public class ThreeConsecutiveOdds {
    public static void main(String[] args) {
        Solution1 s = new Solution1();
        boolean output = s.threeConsecutiveOdds(new int[]{1,2,34,3,4,5,7,23,12});
        System.out.println(output);
    }
}

class Solution1 {
    public boolean threeConsecutiveOdds(int[] arr) {
     
        int odd = 0;
        for (int i : arr) {
            if(i % 2 == 1){
                odd++;
                if(odd >= 3)
                    return true;
            }
            else
                odd = 0;

            }
            if(odd >= 3)
                return true;
            else
                return false;
    
        }
}