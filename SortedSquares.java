import java.util.Arrays;

public class SortedSquares {
    //Done
    public static void main(String[] args) {
        Solution s= new Solution();
        int[] numbers = new int[]{-1,0,-3,10,4};
        int[] inr = s.sortedSquares(numbers);
        System.out.println(inr);       
    }
}

class Solution {
    public int[] sortedSquares(int[] nums) {
        
        int[] squares = new int[nums.length];
     
        int j = 0;
        for (int i : nums) {
            i = i * i;
            nums[j] = i;            
            j++;
        }
        Arrays.sort(nums);
        for (int i : nums) {
            System.out.println(i);    
        }
        
        return nums;
    }
}