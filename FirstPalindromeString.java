public class FirstPalindromeString {
    //Done
    public static void main(String[] args) {
        Solution s = new Solution();
        String output = s.firstPalindrome(new String[] {"abc","car","ada","racecar","cool"});
        System.out.println(output);
    }
}

class Solution {
    public String firstPalindrome(String[] words) {
        
        for (String string : words) {
            StringBuilder sb = new StringBuilder();
            sb.append(string);
            sb.reverse();
            if(string.equals(sb.toString())){
                return string;
            }
        }
        return "";
    }
}
